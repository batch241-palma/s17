/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
		*/

		function completeDetails() {
			let fullName = prompt("What is your full name?");
			let getAge = prompt("What is your age?");
			let getAddress = prompt("What is your complete address?");
			console.log("Hello, " + fullName);
			console.log("Your are " + getAge + " years old.");
			console.log("You live in " + getAddress);
		}

		completeDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
		*/

		function favoriteBands() {
			console.log("1. U2");
			console.log("2. The Calling"); 
			console.log("3. Eraserheads"); 
			console.log("4. Parokya ni Edgar"); 
			console.log("5. Kamikazee"); 
		}

		favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
		*/

		function favoriteMovies() {
			console.log("1. Facing the Giants");
			console.log("Rotten Tomatoes Rating: 85%");
			console.log("2. Remember the Titans");
			console.log("Rotten Tomatoes Rating: 72%");
			console.log("3. Armageddon");
			console.log("Rotten Tomatoes Rating: 73%");
			console.log("4. Pay it Forward");
			console.log("Rotten Tomatoes Rating: 77%");
			console.log("5. The Pursuit of Happyness");
			console.log("Rotten Tomatoes Rating: 87%");
		}

		favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		*/

		// printUsers();
		let printFriends = function printUsers(){
			alert("Hi! Please add the names of your friends.");
			let friend1 = prompt("Enter your first friend's name:"); 
			let friend2 = prompt("Enter your second friend's name:"); 
			let friend3 = prompt("Enter your third friend's name:");

			console.log("You are friends with:")
			console.log(friend1); 
			console.log(friend2); 
			console.log(friend3); 
		};
		printFriends();
		// console.log(friend1);
		// console.log(friend2);
