//alert("Hello!");

/*
FUNCTIONS
	Functions in javascript are lines/block of codes that tell our device/application to perform a certain task when called/invoked

	Functions are mostly created to create complicated tasks to run several lines of code in succession

	
	Function are also used to prevent repeating lines/blocks of codes that perform the same task/function
*/

//Function Declaration
	//(function statement) defines a function with the specified parameters

/*

	Syntax:
		function functionName() {
			codeblock(statement)
		}

	function keyword - used to defined a javascript functions
	functionName - the function name.Function are named to be able to use later in the code
	function block ({}) - the statements which comprise the body of the function. This is where the code to be executed

*/

console.log("My name is John Outside!");

function printName() {
	console.log("My name is John!");
}


//Function Invocation
/*
		The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
*/

//Invoke/call the function that we declared
printName();

//declaredFunction(); //Uncaught ReferenceError: declaredFunction is not defined


//Function Declaration vs Function Expressions
	
	//Function Declaration
		//A function can be created through function declaration by using the function keyword and adding the function name
		//Declared function are NOT executed immediately. They are "saved for later used" and will be executed later when they are invoked(called).
	
	//Hoisting
	declaredFunction();

	function declaredFunction() {
		console.log("Hello World from the declaredFunction()");
	}

	declaredFunction();


	//Function Expressions
			// A function can also be stored in a variable. This is called function expression

			//A function expression is an anonymous function assigned to the variableFunction

			//Anonymous function - a function without a name


	//variableFunction(); //Uncaught ReferenceError: Cannot access 'variableFunction' before initialization

	let variableFunction = function() {

		console.log("Hello from the variableFunction");
	}

	variableFunction();


//Function expression are ALWAYS invoked(called) using the variable name.

	let funcExpression = function funcName() {
		console.log("Hello from the other side!");
	}

	funcExpression();
	//funcName();


//Can we reassign declared functions and function expressions to a NEW anonymous functions? YES!!

	declaredFunction = function() {
		console.log("Updated declaredFunction");
	}

	declaredFunction();

	funcExpression = function () {
		console.log("Updated funcExpression");
	}

	funcExpression();


	//We cannot re-assigned a function expression initialized with const
	const constantFunc = function() {
		console.log("Initialized with CONST");
	}

	constantFunc();

	// constantFunc = function(){
	// 	console.log("Can we reassigned?");
	// }

	// constantFunc();
	// Uncaught TypeError: Assignment to constant variable.


//Function Scoping
/*
	Scope is the accessibility (visibility) of variables
	
	JS Variables has 3 types of scope
		1. local/block scope
		2. global scope
		3, function scope

*/

{
	let localVar = "I am a local variable."
}

let globalVar = "I am a global variable."

console.log(globalVar);
//console.log(localVar); //Uncaught ReferenceError: localVar is not defined
  

  function showNames() {

  	//Function scoped variables
  	var functionVar = "Joe";
  	const functionConst = "John";
  	let functionLet = "Jane";

  	console.log(functionVar);
  	console.log(functionConst);
  	console.log(functionLet)
  }

  showNames();

//The variables, functionvar,functionConst and functionLet are function scoped variables. They can only be accessed inside of the function they were declared in.
  	// console.log(functionVar);
  	// console.log(functionConst);
  	// console.log(functionLet);



//NESTED FUNCTION
	//You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable "name" as they are within the same code block/scope

	function myNewFunction() {
		let name = "Maria";

		function nestedFunction() {
			let nestedName = "Jose";
			console.log(name);

		}

		//console.log(nestedName); index.js:174 Uncaught ReferenceError: nestedName is not defined

		nestedFunction();
	}

	myNewFunction();
	//nestedFunction(); this will cause an error


//Function and Global Scoped Variables

	//Global Variable
	let globalName = "Erven Joshua";

	function myNewFunction2() {
		let nameInside = "Jenno";

		console.log(globalName);
	}

	myNewFunction2();

	//console.log(nameInside);


//USING ALERT()
	////alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.


/*
	Syntax:
		alert(message)

*/

	//alert("Hello World!"); //This will run immediately when the page loads.

	//You can use an alert() to show a message to the user from a later function invocation.
	function showSampleAlert() {
		alert("Hello User!")
	}

	showSampleAlert();

	//You will find that the page waits for the user to dismiss the dialog before proceeding. You can witness this by reloading the page while the console is open.
	console.log("I will only log in the console when the alert is dismissed!");

	//Notes on the use of alert():
		//Show only an alert() for short dialogs/messages to the user. 
		//Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.


//USING PROMPT()
	////prompt() allows us to show a small window at the of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.



	/*
		Syntax:
			prompt("<DialogInString>");


	*/

	let samplePrompt = prompt("Enter your name:");
	////console.log(typeof samplePrompt);//The value of the user input from a prompt is returned as a string.
	console.log("Hello, " + samplePrompt);
	console.log(typeof samplePrompt);

	//prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().

	//prompt() can be used to gather user input and be used in our code. However, since prompt() windows will have the page wait until the user dismisses the window it must not be overused.

	//prompt() used globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them in a function.

	//Let's create function scoped variables to store the returned data from our prompt(). This way, we can dictate when to use a prompt() window or have a reusable function to use our prompts.
	function printWelcomeMessage() {
		let firstName = prompt("Enter your First Name:");
		let lastName = prompt("Enter your Last Name");

		// console.log("Hello " + firstName + ' '+ lastName+ "!");
		// console.log("Welcome to my page!");
		alert("Hello " + firstName + ' '+ lastName+ "!");
		alert("Welcome to my page!");
	}

	printWelcomeMessage()


//Function Naming Conventions
	//Name your functions in small caps. Follows camelCase when naming variables and functions

	function displayCarInfo() {
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1, 500, 000");
	}

	displayCarInfo();

	//Function names should be definitive of the task it will perform. It usually contains a verb

	function getCourse() {
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}

	getCourse();

	//Avoid generic names to avoid confusion within your code

	//getName
	function get() {
		let name = "Jamie";
		console.log(name);
	}

	get();

	//Avoid pointless and inappropriate function names
	function foo() {
		console.log(25 % 5);
	}

	foo();
